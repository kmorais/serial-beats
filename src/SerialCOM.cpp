#include <QDebug>
#include "SerialCOM.h"
#include <QThread>

// *****************************************************************************
SerialCOM::SerialCOM(){
  m_status    = TConStatus::closed;
  m_pFileInfo = nullptr;

  // configure serial port
  m_serialPort.setReadBufferSize(0);  //0= unlimited

  QObject::connect(&m_readTimer, SIGNAL(timeout()), this, SLOT(slots_timeOutRead()));
  QObject::connect(&m_sendTimer, SIGNAL(timeout()), this, SLOT(slots_sendTimeOut()));
  // m_sendTimer

  // connect chunk signals as "queued", since we do not want to block the event loop
  QObject::connect(this, SIGNAL(signals_writeFileChunk(FileInfo*)), this, SLOT(writeFileChunk(FileInfo*)), Qt::ConnectionType::QueuedConnection);
}

SerialCOM::~SerialCOM(){
  if (m_serialPort.isOpen())
    m_serialPort.close();
}

// *****************************************************************************
// Centralized method to change inner status value, and emit a signal
void SerialCOM::changeStatus(TConStatus newStatus){
  if (newStatus != m_status){
    qDebug() << "[LOG] SerialCOM::changeStatus(): " << ConStatusToString(m_status) << " -> " << ConStatusToString(newStatus);
    m_status = newStatus;
    emit signals_status((int)newStatus);
  }
}

// *****************************************************************************
// Read at each X ms data received, while also working as an alive, since we
// check if the QSerialPort object still returns true for isOpen()
void SerialCOM::slots_timeOutRead(){
  #ifdef SERIAL_BEATS_DISC_DEBUG
    if (true) {
  #else
    if ((m_status == TConStatus::connected) & (m_serialPort.isOpen())) {
  #endif
    // read and signals the new data (if any)
    QByteArray* pData = new QByteArray();

    #ifdef SERIAL_BEATS_DISC_DEBUG
      pData->append("a b c d e f g h i\n0 1 2 3 4 5 6 7 8 9\n");
    #else
      *pData = m_serialPort.readAll();
    #endif

    if (!pData->isEmpty()){
      qDebug() << "[LOG] SerialCOM::slots_timeOutRead(): pData().Size=" << pData->size();
      emit signals_dataAvailable(pData);
    }
    else{
      delete pData;

      // close connection if not available anymore
      // ResourceError = device unexpectedly removed from the system
      // TODO: check this error is being shown when sending file chunks
      // if (m_serialPort.error() == QSerialPort::ResourceError){
      //   close();}
    }
  }
}

// *****************************************************************************
void SerialCOM::connect(SerialConfig* pConfig){
  if (m_status == TConStatus::closed) {
    m_serialPort.setBaudRate(pConfig->baudRate);
    m_serialPort.setParity((QSerialPort::Parity)pConfig->parity);
    m_serialPort.setStopBits((QSerialPort::StopBits)pConfig->stopBits);
    m_serialPort.setFlowControl((QSerialPort::FlowControl)pConfig->flowControl);
    m_serialPort.setPortName(pConfig->portName);

    #ifdef SERIAL_BEATS_DISC_DEBUG
      if(true){
    #else
      if (m_serialPort.open(QIODevice::ReadWrite)){
    #endif
      // starts timer and change current status
      m_readTimer.start(m_readDelay); //100ms
      changeStatus(TConStatus::connected);
      qDebug() <<
      "[LOG] SerialCOM::connect(): \n" <<
      "     baudRate=" << m_serialPort.baudRate()   <<"\n"<<
      "       parity=" << m_serialPort.parity()     <<"\n"<<
      "   oneStopBit=" << m_serialPort.stopBits()   <<"\n"<<
      "  flowControl=" << m_serialPort.flowControl()<<"\n"<<
      "     portName=" << m_serialPort.portName();
    }
    else{
      qDebug() << "[ERROR] SerialCOM::connect(): " << m_serialPort.errorString() << " : portName=" << pConfig->portName;}
  }

  // delete pointers
  delete pConfig;
}

// *****************************************************************************
// Centralized method to stop any logic around data begin sent/received
void SerialCOM::close(){
  m_serialPort.close();
  m_readTimer.stop();
  changeStatus(TConStatus::closed);
}

// *****************************************************************************
void SerialCOM::write(QByteArray* pData){
  if (m_status == TConStatus::connected)
    m_serialPort.write(*pData);

  delete pData;
}

// *****************************************************************************
void SerialCOM::writeFileChunk(FileInfo* pInfo){

  if (pInfo != nullptr) {
    // NOTE: small buffer, so the event loop do not freeze, QFile does some inner
    // buffering for performance improvement
    const int  MAX_BUFFER_SIZE = 1*1024; //1kiB
    bool       bDone           = true;
    bool       startTimer      = true;

    m_pFileInfo = pInfo;  //assign class reference of the file info

    if (m_status == TConStatus::connected){
      // fill buffer with data from disk
      QByteArray* pChunk = new QByteArray;

      // read X bytes, according to the delay type and value
      if ((m_pFileInfo->delayType == TDelayType::none) | (m_pFileInfo->delay == 0)) {
        //default = read without any delay
        *pChunk    = m_pFileInfo->pFile->read(MAX_BUFFER_SIZE);
        startTimer = false;
      }
      else if (m_pFileInfo->delayType == TDelayType::byte) {
        //read with X ms delay for each byte
        *pChunk = m_pFileInfo->pFile->read(1);}
      else {
        //read a whole line
        *pChunk = m_pFileInfo->pFile->readLine();
      }

      if (pChunk->size() > 0){
        m_serialPort.write(*pChunk);  //send to the connection thread
        while (m_serialPort.bytesToWrite() > 0){
          bool flush_result = m_serialPort.flush(); //do not wait for the return of the event loop to write
          qDebug() << "[LOG] SerialCOM::writeFileChunk(): flush=" << flush_result << " : bytesToWrite()=" << m_serialPort.bytesToWrite();
        };

        FileProgress* pNotifyInfo = new FileProgress;
        pNotifyInfo->pLastChunk = pChunk;

        if (m_pFileInfo->pFile->atEnd()){
          pNotifyInfo->total = 100;} //make sure we send 100% status
        else{
          pNotifyInfo->total = (100 * m_pFileInfo->pFile->pos())/m_pFileInfo->pFile->size();}

        // notify parent thread about the newly sent data
        emit signals_fileChunkWritten(pNotifyInfo);

        // emit a new signal for the next chunk of data (if not done yet)
        if (!m_pFileInfo->pFile->atEnd()){
          bDone = false;

          // use delayed send if configured
          if (startTimer){
            m_sendTimer.start(m_pFileInfo->delay);}
          else{
            emit signals_writeFileChunk(m_pFileInfo);}
        }
      }
      else{
        delete pChunk;}
    }

    // close file and release any resources
    if (bDone){
      releaseFileInfo();}
  }
}

// *****************************************************************************
void SerialCOM::fileCancel(){
  if (!releaseFileInfo()){
    qDebug() << "[ERROR] SerialCOM::fileCancel(): failed";}
  else {
    // send a progress of 100%, so the transfer is set as done
    FileProgress* pNotifyInfo = new FileProgress;
    pNotifyInfo->pLastChunk = nullptr;
    pNotifyInfo->total      = 100;
    emit signals_fileChunkWritten(pNotifyInfo);
  }
}

// *****************************************************************************
void SerialCOM::setReadDelay(int newDelay){
  if (m_readDelay != newDelay){
    qDebug() << "[LOG] SerialCOM::setReadDelay(): " << m_readDelay << " -> " << newDelay;
    m_readDelay = newDelay;

    // restart timer if active
    if (m_readTimer.isActive()){
      m_readTimer.start(m_readDelay);
    }
  }
}

// *****************************************************************************
void SerialCOM::slots_sendTimeOut(){
  // send next chunk from the info stored at the class pointer,
  if (m_pFileInfo != nullptr){
    // set class pointer to nullptr, writeFileChunk will set it again if needed
    FileInfo* pAuxInfo = m_pFileInfo;
    m_pFileInfo        = nullptr;

    writeFileChunk(pAuxInfo);}
  else{
    m_sendTimer.stop();}
}

// *****************************************************************************
bool SerialCOM::releaseFileInfo(){
  if (m_pFileInfo != nullptr){
    m_pFileInfo->pFile->close();
    delete m_pFileInfo->pFile;
    delete m_pFileInfo;
    m_pFileInfo = nullptr;

    return true;
  }
  else{
    return false;}
}
