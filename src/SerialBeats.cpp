/*
  Copyright (C) 2019-2020, Kevin P. Morais <moraiskv@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// *****************************************************************************
#include <QApplication>
#include <QMessageBox>
#include <QtWidgets/QWidget>
#include <QDir>
#include <QRect>
#include <QResizeEvent>
#include <QMenu>
#include <QFileDialog>
#include <QSettings>
#include <QDebug>

#include "SerialBeats.h"
#include "DiskSettings.h"

// *****************************************************************************
SerialBeats::SerialBeats(QWidget *parent) :
  QMainWindow(parent)
{
  qDebug() << "[LOG] SerialBeats::SerialBeats(): " << thread()->currentThread() << " : start";

  m_status       = TConStatus::closed;
  m_sendingFile  = false;
  m_lastFileName = HOME_DIR;

  // initialize ui elements (in order)
  initializeMainWindow();           //1
  initializeComSettings();          //2
  initializeAboutQt();              //3
  initializeSignalsToSlots();       //4

  // trigger some initial ui adjustments
	portReloadButtonClicked();        // initial search for ports
  dataLayoutComboBoxChanged(m_pUi->dataLayoutComboBox->currentIndex());
  rxFormatComboBoxChanged(m_pUi->rxFormatComboBox->currentIndex());

  // initialize connection (worker) thread
  m_conThread.start();              // initialize connection thread execution
  while(!m_conThread.isRunning()){};// wait for it to be ready

  // Dummy data to test some features like save button or data format without a connection
  #ifdef SERIAL_BEATS_DISC_DEBUG
    m_pUi->rxPlainText->insertPlainText("0123456789\nABCDEFGHIJ\n0123456789\nABCDEFGHIJ");
  #endif
}

// *****************************************************************************
SerialBeats::~SerialBeats() {
  // save settings before destroying any ui element
  saveSettings();

  // delete ui
  delete m_pUi;

  // stop child threads
  m_conThread.quit();
  m_conThread.wait();

  qDebug() << "[LOG] SerialBeats::SerialBeats(): " << thread()->currentThread() << " : end";
}

// *****************************************************************************
void SerialBeats::initializeMainWindow() {
  m_pUi = new Ui_MainWindow;
  m_pUi->setupUi(this);

  // backup the current stylesheet
  m_portButtonStyle = m_pUi->portStatusButton->styleSheet();

  // set file progress widget style, and hide it
  QString strStyle = "QWidget#fileProgressWidget {";
  strStyle.append("border: 1px solid rgb(0, 200, 255); border-radius: 5px;");
  strStyle.append("background-color: rgba(0, 200, 255, 10);}");
  m_pUi->fileProgressWidget->setStyleSheet(strStyle);
  m_pUi->fileProgressWidget->hide();

  // add custom style to the text panels
  m_pUi->txPlainText->setStyleSheet("QPlainTextEdit {background-color: rgb(22, 17, 17);}");
  m_pUi->rxPlainText->setStyleSheet("QPlainTextEdit {background-color: rgb(22, 17, 17);}");

  // configure layout icons
  m_pUi->dataLayoutComboBox->setItemIcon(0, QIcon::fromTheme(QString::fromUtf8("view-left-close")));
  m_pUi->dataLayoutComboBox->setItemIcon(1, QIcon::fromTheme(QString::fromUtf8("view-split-left-right")));
  m_pUi->dataLayoutComboBox->setItemIcon(2, QIcon::fromTheme(QString::fromUtf8("view-split-top-bottom")));
}

// *****************************************************************************
// Initialize settings UI
void SerialBeats::initializeComSettings() {
  // set baudrate options
  m_pUi->baudrateComboBox->clear();
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud1200),   QSerialPort::Baud1200);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud2400),   QSerialPort::Baud2400);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud4800),   QSerialPort::Baud4800);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud9600),   QSerialPort::Baud9600);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud19200),  QSerialPort::Baud19200);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud38400),  QSerialPort::Baud38400);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud57600),  QSerialPort::Baud57600);
  m_pUi->baudrateComboBox->addItem(QString::number(QSerialPort::Baud115200), QSerialPort::Baud115200);

  // set parity options
  m_pUi->parityComboBox->addItem("8N1 - no parity",   QSerialPort::NoParity);
  m_pUi->parityComboBox->addItem("7E1 - even parity", QSerialPort::EvenParity);
  m_pUi->parityComboBox->addItem("7E1 - odd parity",  QSerialPort::OddParity);
  m_pUi->parityComboBox->setCurrentIndex(0);

  // set stop bit options
  m_pUi->stopBitComboBox->addItem("1 bit",  QSerialPort::StopBits::OneStop);
  m_pUi->stopBitComboBox->addItem("2 bits", QSerialPort::StopBits::TwoStop);
  m_pUi->stopBitComboBox->setCurrentIndex(0);

  // set flow control options
  m_pUi->flowControlComboBox->addItem("None",       QSerialPort::FlowControl::NoFlowControl);
  m_pUi->flowControlComboBox->addItem("Software",   QSerialPort::FlowControl::SoftwareControl);
  m_pUi->flowControlComboBox->addItem("2 Hardware", QSerialPort::FlowControl::HardwareControl);
  m_pUi->flowControlComboBox->setCurrentIndex(0);

  // set file tx header options
  m_pUi->fileTxHeaderComboBox->addItem("None");
  m_pUi->fileTxHeaderComboBox->addItem("File Size (4 Bytes)");
  m_pUi->fileTxHeaderComboBox->setCurrentIndex(0);

  // set delay type options
  m_pUi->fileDelayComboBox->addItem("None", TDelayType::none);
  m_pUi->fileDelayComboBox->addItem("Byte", TDelayType::byte);
  m_pUi->fileDelayComboBox->addItem("Line", TDelayType::line);
  m_pUi->fileDelayComboBox->setCurrentIndex(0);

  // load settings from disk
  loadSettings();
}

// *****************************************************************************
// Initialize about window
void SerialBeats::initializeAboutQt() {
  // create action, and then the menu to add it
  m_pAboutQtAction = new QAction(this);
  m_pAboutQtAction->setObjectName(QString::fromUtf8("about_qt_action"));
  m_pAboutQtAction->setText("about Qt");

  m_pAboutMenu = new QMenu(this);
  m_pAboutMenu->addAction(m_pAboutQtAction);

  // set new menu into the button found inside the settings ui
  m_pUi->aboutButton->setMenu(m_pAboutMenu);
}

// *****************************************************************************
// Initialize each signal to slot
void SerialBeats::initializeSignalsToSlots() {
  // main window
  connect(m_pUi->connectButton,     SIGNAL(clicked()),                this,                         SLOT(connectButtonClicked()));
  connect(m_pUi->saveButton,        SIGNAL(clicked()),                this,                         SLOT(saveButtonClicked()));
  connect(m_pUi->sendFileButton,    SIGNAL(clicked()),                this,                         SLOT(sendFileButtonClicked()));
  connect(m_pUi->sendLineEdit,      SIGNAL(returnPressed()),          m_pUi->sendButton,    SIGNAL(clicked()));
  connect(m_pUi->sendButton,        SIGNAL(clicked()),                this,                         SLOT(sendButtonClicked()));
  connect(m_pUi->clearButton,       SIGNAL(clicked()),                m_pUi->txPlainText,   SLOT(clear()));
  connect(m_pUi->clearButton,       SIGNAL(clicked()),                m_pUi->rxPlainText,   SLOT(clear()));
  connect(m_pUi->portStatusButton,  SIGNAL(clicked()),                m_pUi->connectButton, SIGNAL(clicked()));
  connect(m_pUi->fileCancelButton,  SIGNAL(clicked()),                this,                         SLOT(fileCancelClicked()));
  connect(m_pUi->dataLayoutComboBox,SIGNAL(currentIndexChanged(int)), this,                         SLOT(dataLayoutComboBoxChanged(int)));
  connect(m_pUi->rxFormatComboBox,  SIGNAL(currentIndexChanged(int)), this,                         SLOT(rxFormatComboBoxChanged(int)));

  // settings
  connect(m_pUi->portComboBox,     SIGNAL(currentTextChanged(QString)), this, SLOT(portComboBoxChanged(QString)));
  connect(m_pUi->portReloadButton, SIGNAL(clicked()) ,                  this, SLOT(portReloadButtonClicked()));
  connect(m_pAboutQtAction,                SIGNAL(triggered()),                 this, SLOT(aboutQtTrigger()));
  connect(m_pUi->readDelaySpinBox, SIGNAL(valueChanged(int)),           this, SLOT(readDelaySpinBoxValueChanged(int)));

  // connection thread slots
  connect(&m_conThread, SIGNAL(signals_status(int)),                     this, SLOT(slots_newConnectionStatus(int)));
  connect(&m_conThread, SIGNAL(signals_dataAvailable(QByteArray*)),      this, SLOT(slots_newConnectionDataAvailable(QByteArray*)));
  connect(&m_conThread, SIGNAL(signals_fileChunkWritten(FileProgress*)), this, SLOT(slots_fileChunkWritten(FileProgress*)));
}

// *****************************************************************************
// Handle an event to show a window "About Qt"
void SerialBeats::aboutQtTrigger() {
	QMessageBox::aboutQt(this , "About - Qt");
}

// *****************************************************************************
// Update panel being visualized
void SerialBeats::dataLayoutComboBoxChanged(int index) {
  // hide tx panel if rx only
  if (index == LAYOUT_RX) {
    m_pUi->txTextWidget->hide();}
  else {
    m_pUi->txTextWidget->show();}

  // set orientation
  if (index == LAYOUT_HOR_TX_RX) {
    m_pUi->dataLayoutSplitter->setOrientation(Qt::Orientation::Horizontal);}
  else{
    m_pUi->dataLayoutSplitter->setOrientation(Qt::Orientation::Vertical);}
}

// *****************************************************************************
// Update some settings from the rx plain text
void SerialBeats::rxFormatComboBoxChanged(int index) {
  if (index != RX_PLAIN_FORMAT) {
    // enable line wrapping, since any other format wont process "new line"
    m_pUi->rxPlainText->setLineWrapMode(QPlainTextEdit::LineWrapMode::WidgetWidth);
    m_pUi->rxPlainText->setWordWrapMode(QTextOption::WrapAnywhere);
  }
  else {
    m_pUi->rxPlainText->setLineWrapMode(QPlainTextEdit::LineWrapMode::NoWrap);
    m_pUi->rxPlainText->setWordWrapMode(QTextOption::NoWrap);}
}

// *****************************************************************************
void SerialBeats::sendFileButtonClicked()
{
  if (m_status == TConStatus::connected) {
    QString auxFileName;
    QFile*  pFile = new QFile;

    // open a dialog to select the file, and try to open it
    auxFileName = QFileDialog::getOpenFileName(this, "Open File", m_lastFileName, "*");
    pFile->setFileName(auxFileName);

    if (pFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
      FileInfo* pInfo = new FileInfo;
      pInfo->delay     = m_pUi->fileDelaySpinBox->value();
      pInfo->delayType = (TDelayType)m_pUi->fileDelayComboBox->currentData().toInt();
      pInfo->pFile     = pFile;

      // sent header first, if enabled
      sendFileHeader(pFile->size());
      m_conThread.requestFileWrite(pInfo);

      m_sendingFile = true;
      updateFileProgress(0);  //enables progress widget by settings it's value to 0 (starting)

      m_lastFileName = auxFileName; //only update if used
    }
    else{
      delete pFile;}
  };
}

// *****************************************************************************
void SerialBeats::sendFileHeader(uint32_t fileSize){
  if (m_pUi->fileTxHeaderComboBox->currentIndex() == 1){
    // allocate a new array to be used inside the connection thread
    QByteArray* pData = new QByteArray;

    // start by the MSB
    for (int i=3; i>=0; i--){
       pData->append(fileSize >> (i*8));}

    m_conThread.requestWrite(pData);
  }
}

// *****************************************************************************
void SerialBeats::saveButtonClicked() {
	// open a qfiledialog to choose the name of the destination log file
	QString fileName	= "serialb.log";  //default 
	fileName = QFileDialog::getSaveFileName(this, "Save File" , HOME_DIR+"/"+fileName , "*");

	// use QFile to create/write a file
  if (fileName != ""){
    QTextStream	fileStream;
    QString     data;
    QFile	      fileHandler(fileName);

    if (fileHandler.open(QIODevice::ReadWrite | QIODevice::Text)) {
      fileStream.setDevice(&fileHandler);

      // get and send text
      data = m_pUi->rxPlainText->toPlainText();
      fileStream << data;

      fileHandler.close();
    }
  }
}

// *****************************************************************************
// Scan for ports
void SerialBeats::portReloadButtonClicked() {
  QList<QSerialPortInfo> portList;

  // clear combobox
	m_pUi->portComboBox->clear();

  // get list of available box and add to the ui combobox
  portList = QSerialPortInfo::availablePorts();

  QList<QSerialPortInfo>::iterator listIterator;
  for (listIterator = portList.begin(); listIterator != portList.end(); ++listIterator) {
    QSerialPortInfo Port = *listIterator;
    m_pUi->portComboBox->addItem(Port.portName());
  }
}

// *****************************************************************************
void SerialBeats::readDelaySpinBoxValueChanged(int value){
  m_conThread.requestSetReadDelay(value);
}

// *****************************************************************************
void SerialBeats::portComboBoxChanged(const QString &text) {
  if ((m_status != TConStatus::connected) & (m_status != TConStatus::connecting)){
    QString newText = "Port: ";
    newText.append(text);
    m_pUi->portStatusButton->setText(newText);
  }
}

// *****************************************************************************
void SerialBeats::connectButtonClicked(){
  if (m_status != TConStatus::closed)
    m_conThread.requestClose();
  else {
    SerialConfig* pConfig = new SerialConfig;

    pConfig->portName = m_pUi->portComboBox->currentText();
    pConfig->portName.detach(); //deep copy, since we are gonna send it to another thread

    pConfig->baudRate    = m_pUi->baudrateComboBox->currentData().toInt();
    pConfig->parity      = m_pUi->parityComboBox->currentData().toInt();
    pConfig->stopBits    = m_pUi->stopBitComboBox->currentData().toInt();
    pConfig->flowControl = m_pUi->flowControlComboBox->currentData().toInt();

    readDelaySpinBoxValueChanged(m_pUi->readDelaySpinBox->value()); //make sure the read timer is updated
    m_conThread.requestConnection(pConfig);
  }
}

// *****************************************************************************
void SerialBeats::sendButtonClicked() {
  if (m_status == TConStatus::connected){

    QString	dataToSend;
    QString	ControlArray[] = {"\n","\r","\r\n","\n\r","\0"};
    int		  index = 4;

    // get current text on lineedit widget (typed by user), then clear it
    dataToSend = m_pUi->sendLineEdit->text();
    m_pUi->sendLineEdit->clear();

    // check if the text is not NULL, then append a ctrl char (if selected)
    if (dataToSend[0] != NULL) {
      index = m_pUi->newLineComboBox->currentIndex();
      dataToSend.append(ControlArray[index]);

      // insert the text in the plaintext for send data, and to some auto scroll
      m_pUi->txPlainText->insertPlainText(dataToSend);
      m_pUi->txPlainText->ensureCursorVisible();

      writeQString(dataToSend);
    }
  };
}

// *****************************************************************************
void SerialBeats::writeQString(QString newData) {
  // allocate a new array to be used inside the connection thread
  QByteArray* pData = new QByteArray;
  *pData = newData.toLocal8Bit();
  m_conThread.requestWrite(pData);
}

// *****************************************************************************
// handle a new status signal from the connection thread, adjusting the ui
void SerialBeats::slots_newConnectionStatus(int newStatus){
  if ((TConStatus)newStatus != m_status) {
    qDebug() << "[LOG] SerialBeats::slots_newConnectionStatus(): " << ConStatusToString(m_status) << " -> " << ConStatusToString((TConStatus)newStatus);
    m_status = (TConStatus)newStatus;
    updateUIState();
  }
}

// *****************************************************************************
void SerialBeats::slots_newConnectionDataAvailable(QByteArray* pData){
  // check for the current index of the TextFormatComboBox,
  // so we can convert the string in to its integer/hexadecimal value
  QString auxData = "";

  if (pData->size() > 0){

    if      (m_pUi->rxFormatComboBox->currentIndex() == RX_HEX_FORMAT) {
      m_pUi->rxPlainText->insertPlainText(pData->toHex(':'));
      m_pUi->rxPlainText->insertPlainText(":"); //append last separator
    }
    else if (m_pUi->rxFormatComboBox->currentIndex() == RX_PLAIN_FORMAT) {
      m_pUi->rxPlainText->insertPlainText(*pData);}
    else {
      // check the size of the word used for integer representation
      int wordSize = 0;
      switch (m_pUi->rxFormatComboBox->currentIndex()) {
        case RX_UINT8_FORMAT  : wordSize = 1; break;
        case RX_UINT16_FORMAT : wordSize = 2; break;
        case RX_UINT32_FORMAT : wordSize = 4; break;
        default               : break;
      }

      // convert each group of wordSize bytes to it's integer ascii representation
      for (int position=0; position < pData->size(); position += wordSize){
        int wordPosition = 0;
        int word         = 0;
        while (wordPosition < wordSize) {
          if ((position+wordPosition >= pData->size())) {
            break;}
          word = word + (pData->at(position+wordPosition) << (wordPosition*8));
          wordPosition++;
        }
        auxData.append(QByteArray::number(word)+':'); //append integer with the separator
      }
      m_pUi->rxPlainText->insertPlainText(auxData);
    }

    m_pUi->rxPlainText->ensureCursorVisible();  // auto scroll
  }

  delete pData;
}

// *****************************************************************************
void SerialBeats::slots_fileChunkWritten(FileProgress* pInfo){
  if (pInfo->pLastChunk != nullptr) {
    m_pUi->txPlainText->insertPlainText(*(pInfo->pLastChunk));
    m_pUi->txPlainText->ensureCursorVisible();  //auto scroll
    delete pInfo->pLastChunk;
  }

  qDebug() << "[LOG] SerialBeats::slots_fileChunkWritten(): total= " << pInfo->total << "%\n";

  updateFileProgress(pInfo->total);
  delete pInfo;

}

// *****************************************************************************
// Update which ui elements are enabled/disabled, considering that bEnable
// is used to tell us that current port is at least "connecting"
void SerialBeats::updateUIState() {
  bool enableTX       = false;  //enable tx elements
  bool enableSettings = true;   //enable settings elements

  if (m_status == connected) {
    m_pUi->connectButton->setText("Disconnect");

    // customize portstatus button style
    m_pUi->portStatusButton->setIcon(CON_ICON);
    QString strStyle = "QPushButton#portStatusButton {";
    strStyle.append("border: 1px solid green; border-radius: 5px;");
    strStyle.append("background-color: rgba(100, 255, 100, 10);}");
    m_pUi->portStatusButton->setStyleSheet(strStyle);

    enableTX       = true;
    enableSettings = false;
  }
  else {
    m_pUi->connectButton->setText("Connect");
    m_pUi->portStatusButton->setIcon(DIS_ICON);
    m_pUi->portStatusButton->setStyleSheet(m_portButtonStyle);//restore, since setIcon() will undo the style

    if (m_status == TConStatus::connecting){
      enableSettings = false;}
  }

  // send elements
  m_pUi->sendFileButton->setEnabled(enableTX);
  m_pUi->sendButton->setEnabled(enableTX);
  m_pUi->sendLineEdit->setEnabled(enableTX);

  // settings elements
  m_pUi->portComboBox->setEnabled(enableSettings);
  m_pUi->portReloadButton->setEnabled(enableSettings);
  m_pUi->baudrateComboBox->setEnabled(enableSettings);
  m_pUi->parityComboBox->setEnabled(enableSettings);
  m_pUi->stopBitComboBox->setEnabled(enableSettings);
  m_pUi->flowControlComboBox->setEnabled(enableSettings);

  updateFileProgress(m_pUi->fileProgressBar->value()); //general method to update file progress elements
}

// *****************************************************************************
// Enable/disable only ui elements which are related to the "send file" feature
void SerialBeats::updateSendFileUIState(){
  m_pUi->fileTxHeaderComboBox->setEnabled(!m_sendingFile);
  m_pUi->fileDelayComboBox->setEnabled(!m_sendingFile);
  m_pUi->fileDelaySpinBox->setEnabled(!m_sendingFile);
}

// *****************************************************************************
void SerialBeats::updateFileProgress(int total){
  // just hide if done
  if ((total >= 100) | (m_status != TConStatus::connected) | (!m_sendingFile)) {
    m_pUi->fileProgressWidget->hide();
    m_sendingFile = false;
    updateSendFileUIState();
  }
  else{
    m_pUi->fileProgressBar->setValue(total);

    if (m_pUi->fileProgressWidget->isHidden()){
      m_pUi->fileProgressWidget->show();
      updateSendFileUIState();
    }
  }
}

// *****************************************************************************
void SerialBeats::fileCancelClicked(){
  m_conThread.requestFileCancel();
}

// *****************************************************************************
void SerialBeats::loadSettings(){
  QSettings settings(SETTINGS_FILE, QSettings::Format::IniFormat);

  // main window
  m_pUi->dataLayoutComboBox->setCurrentIndex(settings.value(SETTINGS_DATA_LAYOUT, 0).toInt());
  m_pUi->rxFormatComboBox->setCurrentIndex(settings.value(SETTINGS_RX_FORMAT, 0).toInt());

  // com settings
  m_pUi->baudrateComboBox->setCurrentIndex(settings.value(SETTINGS_BAUDRATE, 0).toInt());
  m_pUi->parityComboBox->setCurrentIndex(settings.value(SETTINGS_PARITY, 0).toInt());
  m_pUi->stopBitComboBox->setCurrentIndex(settings.value(SETTINGS_STOP_BITS, 0).toInt());
  m_pUi->flowControlComboBox->setCurrentIndex(settings.value(SETTINGS_FLOW_CONTROL, 0).toInt());
  m_pUi->fileTxHeaderComboBox->setCurrentIndex(settings.value(SETTINGS_FILE_TX_HEADER, 0).toInt());
  m_pUi->fileDelayComboBox->setCurrentIndex(settings.value(SETTINGS_FILE_DELAY_TYPE, 0).toInt());
  m_pUi->fileDelaySpinBox->setValue(settings.value(SETTINGS_FILE_DELAY_MS, 1).toInt());
  m_pUi->readDelaySpinBox->setValue(settings.value(SETTINGS_READ_DELAY_MS, 100).toInt());

  // settings
  m_lastFileName = settings.value(SETTINGS_LAST_FILE_SENT, HOME_DIR).toString();
}

// *****************************************************************************
void SerialBeats::saveSettings(){
  QSettings settings(SETTINGS_FILE, QSettings::Format::IniFormat);

  settings.clear();

  // main window
  settings.setValue(SETTINGS_DATA_LAYOUT,     m_pUi->dataLayoutComboBox->currentIndex());
  settings.setValue(SETTINGS_RX_FORMAT,       m_pUi->rxFormatComboBox->currentIndex());

  // com settings
  settings.setValue(SETTINGS_BAUDRATE,        m_pUi->baudrateComboBox->currentIndex());
  settings.setValue(SETTINGS_PARITY,          m_pUi->parityComboBox->currentIndex());
  settings.setValue(SETTINGS_STOP_BITS,       m_pUi->stopBitComboBox->currentIndex());
  settings.setValue(SETTINGS_FLOW_CONTROL,    m_pUi->flowControlComboBox->currentIndex());
  settings.setValue(SETTINGS_FILE_TX_HEADER,  m_pUi->fileTxHeaderComboBox->currentIndex());
  settings.setValue(SETTINGS_FILE_DELAY_TYPE, m_pUi->fileDelayComboBox->currentIndex());
  settings.setValue(SETTINGS_FILE_DELAY_MS,   m_pUi->fileDelaySpinBox->value());
  settings.setValue(SETTINGS_READ_DELAY_MS,   m_pUi->readDelaySpinBox->value());

  // settings
  settings.setValue(SETTINGS_LAST_FILE_SENT, m_lastFileName);
}
