#include <QDebug>

#include "ConnectionThread.h"

// *****************************************************************************
ConnectionThread::ConnectionThread(){}

ConnectionThread::~ConnectionThread(){}

// *****************************************************************************
void ConnectionThread::run(){
  qDebug() << "[LOG] ConnectionThread::run(): " << QThread::currentThread() << " : start";

  // create object directly into the worker thread
  SerialCOM connector;

  // set signal<->slot
  connect(this, SIGNAL(signals_requestConnection(SerialConfig*)), &connector, SLOT(connect(SerialConfig*)));
  connect(this, SIGNAL(signals_requestClose()),                   &connector, SLOT(close()));
  connect(this, SIGNAL(signals_requestWrite(QByteArray*)),        &connector, SLOT(write(QByteArray*)));
  connect(this, SIGNAL(signals_requestFileWrite(FileInfo*)),      &connector, SLOT(writeFileChunk(FileInfo*)));
  connect(this, SIGNAL(signals_requestFileCancel()),              &connector, SLOT(fileCancel()));
  connect(this, SIGNAL(signals_requestSetReadDelay(int)),         &connector, SLOT(setReadDelay(int)));

  connect(&connector, SIGNAL(signals_status(int)),                     this, SIGNAL(signals_status(int)));
  connect(&connector, SIGNAL(signals_dataAvailable(QByteArray*)),      this, SIGNAL(signals_dataAvailable(QByteArray*)));
  connect(&connector, SIGNAL(signals_fileChunkWritten(FileProgress*)), this, SIGNAL(signals_fileChunkWritten(FileProgress*)));

  // loop until quit/exit is called
  exec();

  qDebug() << "[LOG] ConnectionThread::run(): " << QThread::currentThread() << " : end";
}

// *****************************************************************************
