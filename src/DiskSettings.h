#ifndef __DISK_SETTINGS_H__
#define __DISK_SETTINGS_H__

#include <QSerialPort>

// *****************************************************************************
#define SETTINGS_FILE             "serialb.ini"

#define SETTINGS_DATA_LAYOUT      "data-layout"
#define SETTINGS_RX_FORMAT        "rx-format"

#define SETTINGS_BAUDRATE         "baudrate"
#define SETTINGS_PARITY           "parity"
#define SETTINGS_STOP_BITS        "stop-bits"
#define SETTINGS_FLOW_CONTROL     "flow-control"
#define SETTINGS_FILE_TX_HEADER   "file-tx-header"
#define SETTINGS_FILE_DELAY_TYPE  "file-delay-type"
#define SETTINGS_FILE_DELAY_MS    "file-delay-ms"
#define SETTINGS_READ_DELAY_MS    "read-delay-ms"

#define SETTINGS_LAST_FILE_SENT   "last-file-sent"

// *****************************************************************************
#endif
