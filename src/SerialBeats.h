/*
  Copyright (C) 2019-2020, Kevin P. Morais <moraiskv@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// *****************************************************************************
#ifndef __SERIAL_BEATS_H__
#define __SERIAL_BEATS_H__

// *****************************************************************************
#include <QMainWindow>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QPropertyAnimation>
#include <QSerialPortInfo>

#include <ui_mainwindow.h>
#include "ConnectionThread.h"
#include "BaseCOM.h"

// *****************************************************************************
// MACROS
// combobox index defines
#define LAYOUT_RX         0
#define LAYOUT_HOR_TX_RX  1 //horizontal
#define LAYOUT_VER_TX_RX  2 //vertical

#define RX_UINT8_FORMAT   0
#define RX_UINT16_FORMAT  1
#define RX_UINT32_FORMAT  2
#define RX_HEX_FORMAT     3
#define RX_PLAIN_FORMAT   4

#define HOME_DIR QDir::homePath()

#define CON_ICON QIcon::fromTheme(QString::fromUtf8("network-connect"))
#define DIS_ICON QIcon::fromTheme(QString::fromUtf8("network-disconnect"))

// *****************************************************************************
class SerialBeats : public QMainWindow
{
  Q_OBJECT // macro for signal-slot interpretations and their syntax

  private slots:
    // buttons
    void connectButtonClicked();
    void saveButtonClicked();
    void sendFileButtonClicked();
    void sendButtonClicked();

    void fileCancelClicked();

    void portComboBoxChanged      (const QString &text);
    void dataLayoutComboBoxChanged(int index);
    void rxFormatComboBoxChanged  (int index);

    void portReloadButtonClicked();
    void readDelaySpinBoxValueChanged(int value);
    void aboutQtTrigger();

    // connection thread slots
    void slots_newConnectionStatus       (int newStats);
    void slots_newConnectionDataAvailable(QByteArray* pData);
    void slots_fileChunkWritten          (FileProgress* pInfo);

  private:
    void initializeMainWindow();
    void initializeComSettings();
    void initializeAboutQt();
    void initializeSignalsToSlots();

    void loadSettings();
    void saveSettings();

    void sendFileHeader(uint32_t fileSize);

    void updateUIState        ();
    void updateSendFileUIState();
    void updateFileProgress   (int total);
    void writeQString         (QString newData);

    Ui_MainWindow     *m_pUi;
    QMenu             *m_pAboutMenu;
    QAction           *m_pAboutQtAction;
    QString           m_portButtonStyle;
    ConnectionThread  m_conThread;
    TConStatus        m_status      = TConStatus::closed;
    bool              m_sendingFile = false;
    QString           m_lastFileName;

  public:
    explicit SerialBeats(QWidget *parent = 0); // constructor
    ~SerialBeats();                            // destructor
};

// ********************************************************
#endif // __SERIAL_BEATS_H__
