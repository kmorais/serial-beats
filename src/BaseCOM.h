// *****************************************************************************
// Author     : Kevin Paula Morais
// File       : BaseCOM.h
// Date       : 21/01/2024
// Description: Set of definitions used for connections
// *****************************************************************************

#ifndef __BASE_COM_H__
#define __BASE_COM_H__

#include <QFile>
#include <QString>

// *****************************************************************************
  enum TConStatus{
    closed      = 0,
    connecting  = 1,
    connected   = 2,
    unknown     = 255
  };

  enum TDelayType {
    none = 0,
    byte = 1,
    line = 2
  };

  struct SerialConfig {
    int     baudRate;
    int     parity;
    int     stopBits;
    int     flowControl;
    QString portName;
  };

  // struct used to carry info about the file being sent through multiples signals
  struct FileInfo {
    int        delay;
    TDelayType delayType;
    QFile*     pFile;
  };

  struct FileProgress {
    int         total = 0;
    QByteArray* pLastChunk;
  };

// *****************************************************************************
  static inline QString ConStatusToString(TConStatus value){
    switch (value) {
      case TConStatus::closed     : return "closed";     break;
      case TConStatus::connecting : return "connecting"; break;
      case TConStatus::connected  : return "connected";  break;
      case TConStatus::unknown    : return "unknown";    break;
      default                     : return "ERROR";      break;
    }
  }

// *****************************************************************************
#endif //__BASE_COM_H__
