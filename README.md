# Serial Beats

Serial Beats is a software written in C++, to performe a serial communication between two devices.

Both the UI and serial port is built with the **_Qt toolkit_**.

<!-- --------------------- -->
## Compile
To generate the executable, run the PRO file from the build folder, and then execute **_Makefile_**.

```
mkdir build
cd build
qmake ..
make
```

The output from the compile process will be placed inside the **build** folder, named **serialb**. If you wish to install the software, run **sudo make install** at the project top directory.

<!-- --------------------- -->
## TODO
- add drag&drop feature to both text widget (to send/save)
- add color to some control char received when not using plain text mode (since they wont be rendered)
- add shortcuts, like <Ctrl+S> (which would trigger the save file button), or <Ctrl+O> (in this case, would trigger the open file button).
- error/exception handling
