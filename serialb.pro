#############################################
# Project                                   #
#############################################

#---
# executable output name
TARGET      = "serialb"

TEMPLATE    = app

#---
# installation

# executable
unix {
    target_system = Unix
    target.path = "/usr/local/bin"
}
win32 {
    target_system = Windows
    target.path = "C:\ProgramFiles\serialb"
}

message("Building on a $$target_system system")
message("Installation path: $$target.path")
INSTALLS += target

# desktop file
unix {
    desktop.path    = "/usr/share/applications"
    desktop.files   = resources/serialb.desktop
}
win32 {
    # desktop.path  = "C:\ProgramFiles\Serial Beats"
    # desktop.files =
}

INSTALLS += desktop

# icon file
unix {
    icon.path   = "/usr/share/pixmaps"
    icon.files  = resources/serialb.png
    INSTALLS += icon
}

#############################################
# GLOBAL MACROS                             #
#############################################
# UNCOMMENT FOR EASIER DEBUG ONLY
#DEFINES += SERIAL_BEATS_DISC_DEBUG=1

#############################################
# Folder Structure                          #
#############################################

#---
# project structure
BUILD_DIR   = build

DESTDIR     = $$BUILD_DIR
OBJECTS_DIR = $$BUILD_DIR/obj
MOC_DIR     = $$BUILD_DIR/moc

UI_DIR      = ui
RCC_DIR     = resources

SOURCES     += $$files(src/*.cpp)
HEADERS     += $$files(src/*.h)
FORMS       += $$files(ui/*.ui)
RESOURCES   += resources/resources.qrc

#############################################
# Compiler                                  #
#############################################

DEFINES += QT_DEPRECATED_WARNINGS

## QtWidgets as separated modules since the transition from Qt4.x to Qt5
QT += widgets
QT += serialport

## build mode
CONFIG += debug
#CONFIG += release
